% Revision Process: Branching and Merging

### Issues with Our Revision Process

Currently we just copy the folder and make changes. This has created multiple problems:

* No history, we can't see how the files have changed.
* Confusion about which versions need to live where.
* Ad hoc revision folder identification. 
* No rollback or automatic archiving.
* Can't share changes between versions. (For example, changing the textbook version needs to happen in both places.)

### Doing It the Git Way: Branch and Merge

* A *branch* is a named set of revisions that are tracked independently of other branches. 
* A branch is an alternate version, like a mirror pocket universe. 
* A branch lives in the same repo. It's easy to switch between versions. 
* You can compare files between versions.
* The main trunk of a repo is called *master*. 
* When you're done with a branch, you *merge* it back into the master.

### Case Example: CMPA 100

* Adobe discontinued Adobe Muse, so we need to rewrite content and assignments that used Adobe Muse. 
	* Units 7 - 10, and a sentence in Unit 1
* Maintain the "live" version until the revision is officially approved. 
* Proposal: Create a branch called "revision\_fall\_2019." Merge after admin review of revision.  

![](images/branch_merge.jpg)

