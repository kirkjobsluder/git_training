% Git Training for SCAD

# What is Git?

* Git is a [Version Control System](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) designed for handling large numbers of "source code" files, including HTML, CSS, and javascript.

* It is extremely well-developed and supported. Apple, Microsoft, and Linux use it. 
* It tracks changes to files on a line-by-line basis. 
* Related changes to multiple files are recorded at the same time. For example, adding an image file. 
* It provides a way to "roll-back" or "undo" changes. 
* It includes logging of changes. Each commit includes:
	* The name and email of the person who committed the change
	* The date and time of the change
	* A comment describing the change
	* An ID that uniquely identifies the change. 
* It handles multiple people working with files at the same time.

# Why do we need it?

Example questions we ask about courses:

* What is the status of the course files? (A: Read the commit log.) 
* Which copy of this course is current? (A: The one in SCAD Bitbucket.)
* Who changed the readings on the syllabus? (A: Read the commit log for the syllabus.)
* Which files have been changed? (A: Check git status.) 


	
# Parts of Our Git System

* git and command-line git tools: Low-level utilities needed for git to work on our computers.
* Dreamweaver and [Sourcetree](https://www.sourcetreeapp.com/): GUIs and Dashboard for working with git files. 

![dreamweaver git history dialog](images/dreamweaver_git_history.png)

![sourcetree interface](images/sourcetree_git.png)

* Bitbucket: A server that stores git repositories and shows information about them. The page for this presentation is [https://bitbucket.org/kirkjobsluder/git_training/src/master/](https://bitbucket.org/kirkjobsluder/git_training/src/master/) Bitbucket adds issue tracking and other features. 
* SCAD Bitbucket: SCAD's self-hosted version of Bitbucket. 

![bitbucket screenshot](images/bitbucket_screenshot.png)

# Basic Vocabulary

## Things

* **Repository:** A repository or "repo" is a collection of files that are all tracked together. They usually represent a single "project." For SCAD I recommend that each course be its own repository.
* **Staged:** The first step in recording a change. First you *stage* a file and its changes, then you *commit* it. 
* **Commit:** *Noun* A set of related changes that are recorded at the same time. *Verb* To record a set of changes to the repository. **Note:** in order to share your work and make it official, you must commit your changes. 
* **Commit ID:** A hexadecimal code that uniquely identifies the commit. This is usually shortened to seven digits. For example: `f862a0c`. (Hexadecimal is the same number system used to specify colors in HTML and CSS.) 
* **Tag:** A named commit, usually done at a project checkpoint. For example: `QA_COMPLETE` or `SPRING_2019`.
* **Remote:** Typically, the version of a repo stored on a server. For us, we should always be using the SCAD Bitbucket. (Unless there's a reason not to.) 

## Actions

* **Clone:** To make a complete copy of a repository and all of its changes. The first step in working with a repo is to *clone* it to your desktop.
* **Pull:** To download a set of changes from the server to your desktop. This only pulls the *changed* parts of the files, so it can be very fast. You usually want to *pull* at the start of a working period. 
* **Push:** To upload a set of changes from your desktop to your remote server. 
* **Merge:** To address editing conflicts causes when two people work on the same project simultaneously. 

# Workflow

## When You Start Work on a Project

1. Clone the repository from SCAD Bitbucket.

## Every Day

1. (Probably good practice.) Check the history for the project on the SCAD Bitbucket web page. 
1. Open the project in Dreamweaver or SourceTree.
2. *Pull* from SCAD Bitbucket.
3. *Merge* if necessary. (Todo: figure out how to do that.)
4. [It's your thing, do what you want to do.](https://youtu.be/Tqc_EhmL8-E)
5. *Stage* your changes. 
6. *Commit* your changes at least daily. Add a good comment. 
7. *Push* to SCAD Bitbucket.

## Good Commit Descriptions

Explain why you made the change, and who requested the changes.

* "Textbook change requested by J. Doe"
* "QA Fixes"

Changes that require detailed explanation should be put in the README.md.

## At Key Checkpoints

Tag your commits at key checkpoints.

* QA_READY
* QA_COMPLETE
* ADMIN_APPROVED
* SPRING_2019 (ready for delivery in the named quarter). 


## Merging

* If two people are working in the same project and commit their changes, the next person to pull will be asked to *merge* changes. 
* Best case: Separate files, no conflict, merge is automatic.
* Worst case: Changes to the same line on file. (TODO: Look up how Dreamweaver handles it.) 
* *Always* push after a merge. 
* Talk about who is changing what during edit-intensive project phases. 
* Pull and commit frequently. 

# Intermediate and Advanced Stuff to Figure Out Later

* Branches and merging (for revision). 
* Automatic deployment.
* How do we setup a project in SCAD Bitbucket.
* Jira integration and issue tracking. 

## Limits to Consider

* Git can't "see" inside binary files (DOCX, JPG, PNG, PDF, PSD, ZIP ...). It just says "the file has changed" and saves both versions. While this shouldn't be a problem for most images, we need to talk about how to work with video assets. 
* Merging can be complicated. Communicate with your co-workers to avoid working on the same files at the same time.
* File access on `Build` and `masterfiles_CS` can be extremely slow. 












